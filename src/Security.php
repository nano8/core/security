<?php

namespace laylatichy\nano\core;

final class Security {
    public static function passwordHash(string $password): string {
        return password_hash(password: $password, algo: PASSWORD_DEFAULT);
    }

    public static function getRandomBytes(int $nbBytes = 32): string {
        do {
            $bytes = openssl_random_pseudo_bytes(length: $nbBytes, strong_result: $strong);
        } while (!$bytes || !$strong);

        return $bytes;
    }

    public static function passwordGenerate(int $length = 10): string {
        do {
            $string = preg_replace(
                pattern: '/[^a-zA-Z0-9]/',
                replacement: '',
                subject: base64_encode(string: self::getRandomBytes(nbBytes: $length + 1))
            );
        } while (!$string);

        return substr(
            string: $string,
            offset: 0,
            length: $length
        );
    }
}
