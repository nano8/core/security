### nano/core/security

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-security?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-security)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-security?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-security)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/security/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/security/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/security/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/security/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/security/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/security/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-security`
